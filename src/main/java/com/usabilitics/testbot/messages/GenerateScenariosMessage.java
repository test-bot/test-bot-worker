package com.usabilitics.testbot.messages;

public class GenerateScenariosMessage {
	private String appId;
	private String accountId;
	private String suiteId;

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the suiteId
	 */
	public String getSuiteId() {
		return suiteId;
	}

	/**
	 * @param suiteId the suiteId to set
	 */
	public void setSuiteId(String suiteId) {
		this.suiteId = suiteId;
	}
}
