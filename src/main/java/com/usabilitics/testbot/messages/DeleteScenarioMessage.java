package com.usabilitics.testbot.messages;

public class DeleteScenarioMessage {
	private String appId;
	private String scenarioId;
	/**
	 * @return the scenarioId
	 */
	public String getScenarioId() {
		return scenarioId;
	}
	/**
	 * @param scenarioId the scenarioId to set
	 */
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
}
