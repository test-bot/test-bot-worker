package com.usabilitics.testbot.messages;

public class PromoteForSessionCalculationMessage {
	private String appId;

	public PromoteForSessionCalculationMessage() {
		
	}
	
	public PromoteForSessionCalculationMessage(String appId) {
		super();
		this.appId = appId;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
}
