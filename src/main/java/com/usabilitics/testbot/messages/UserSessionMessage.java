package com.usabilitics.testbot.messages;

import com.usabilitics.testbot.jobs.sessionsCalculator.UserSession;

public class UserSessionMessage {
	private String userId;
	private long startDate;
	private long endDate;
	private String appId;

	public UserSessionMessage() {
	}

	public UserSessionMessage(UserSession session) {
		this.startDate = session.getStartDate();
		this.endDate = session.getEndDate();
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAppId() {
		return this.appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
}
