package com.usabilitics.testbot.messages;

import java.util.List;

public class ChangedSettingsMessage {
	private List<String> keys;

	public ChangedSettingsMessage(){
	}

	public List<String> getKeys() {
		return keys;
	}

	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
}
