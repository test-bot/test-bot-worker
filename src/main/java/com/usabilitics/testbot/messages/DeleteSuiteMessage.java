package com.usabilitics.testbot.messages;

public class DeleteSuiteMessage {
    private String suiteId;
    private String appId;

	/**
	 * @return the suiteId
	 */
	public String getSuiteId() {
		return suiteId;
	}
	
	/**
	 * @param suiteId the suiteId to set
	 */
	public void setSuiteId(String suiteId) {
		this.suiteId = suiteId;
	}
	
	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}
	
	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
}
