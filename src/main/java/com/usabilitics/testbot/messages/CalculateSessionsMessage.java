package com.usabilitics.testbot.messages;

public class CalculateSessionsMessage {
	private String userId;
	private String appId;

	public CalculateSessionsMessage() {

	}

	public CalculateSessionsMessage(String appId, String userId) {
		this.setAppId(appId);
		this.setUserId(userId);
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
}
