package com.usabilitics.testbot.settings;

import static com.mongodb.client.model.Filters.eq;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;

@Named
public class Settings {
	private static Settings instance;

	private MongoCollection<Document> settingsCollection;

	@Inject
	public Settings(DatabaseClient dbClient){
		this.settingsCollection = dbClient.getClient()
				.getDatabase("test-bot")
				.getCollection("settings");
	}

	public <T> T get(String settingKey, T defaultValue) {
		Document setting = settingsCollection.find(eq("key", settingKey)).first();
		T value = defaultValue;
		if(setting == null) {
			settingsCollection.insertOne(
					(new Document("key", settingKey)
							.append("value", defaultValue)));
		}
		else {
			value = (T)setting.get("value");
		}
		
		return value;
	}
}
