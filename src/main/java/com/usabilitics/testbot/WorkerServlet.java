package com.usabilitics.testbot;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.neo4j.ogm.session.Session;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.jobs.TaskScheduler;
import com.usabilitics.testbot.jobs.applicationBuilder.ApplicationWorker;
import com.usabilitics.testbot.jobs.dataGraphBuilder.DataGraphBuilder;
import com.usabilitics.testbot.jobs.scenariosGenerator.ScenariosGenerator;
import com.usabilitics.testbot.jobs.scenariosGenerator.ScenariosWorker;
import com.usabilitics.testbot.jobs.scenariosGenerator.SuitesWorker;
import com.usabilitics.testbot.jobs.sessionsCalculator.PromoteForSessionCalculation;
import com.usabilitics.testbot.jobs.sessionsCalculator.PromoteForSessionCalculationTask;
import com.usabilitics.testbot.jobs.sessionsCalculator.SessionsCalculator;
import com.usabilitics.testbot.jobs.settings.SettingsChangesProcessor;
import com.usabilitics.testbot.jobs.testDataOperations.InputDataRequirementsRecognizer;
import com.usabilitics.testbot.messages.CalculateSessionsMessage;
import com.usabilitics.testbot.messages.ChangedSettingsMessage;
import com.usabilitics.testbot.messages.CreateApplicationMessage;
import com.usabilitics.testbot.messages.DeleteApplicationMessage;
import com.usabilitics.testbot.messages.DeleteScenarioMessage;
import com.usabilitics.testbot.messages.DeleteSuiteMessage;
import com.usabilitics.testbot.messages.GenerateScenariosMessage;
import com.usabilitics.testbot.messages.PromoteForSessionCalculationMessage;
import com.usabilitics.testbot.messages.RecognizeInputDataRequirementsMessage;
import com.usabilitics.testbot.messages.UserSessionMessage;
import com.usabilitics.testbot.settings.Settings;

/**
 * Startup and queue messages entry point.
 *
 */
public class WorkerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	private static final ObjectMapper MAPPER = new ObjectMapper();

	private MongoClient mongoClient;
	private Session graphSession;
	private Settings settings;

	private ApplicationContext appContext;

	/**
	 * Start all tasks that the worker will perform
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		this.appContext = (ApplicationContext)getServletContext().getAttribute("applicationContext");

		this.settings = (Settings)this.appContext.getBean("settings");

		DatabaseClient dbClient = (DatabaseClient)this.appContext.getBean("databaseClient");
		this.mongoClient = dbClient.getClient();

		GraphSessionFactory sessionFactory = (GraphSessionFactory)this.appContext.getBean("graphSessionFactory");
		this.graphSession = sessionFactory.getSession();

		this.startScheduledTasks();
	}

	/**
	 * This method is invoked to handle POST requests from the local SQS daemon
	 * when a work item is pulled off of the queue. The body of the request
	 * contains the message pulled off the queue.
	 */
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		try {
			WorkTypes workType = WorkTypes.valueOf(request.getHeader("X-Aws-Sqsd-Attr-WorkType"));

			PromoteForSessionCalculation promoteForSessionCalculation = (PromoteForSessionCalculation)this.appContext.getBean("promoteForSessionCalculation");
			DataGraphBuilder graphTask = (DataGraphBuilder)this.appContext.getBean("dataGraphBuilder");
			SessionsCalculator sessionsCalculator = (SessionsCalculator)this.appContext.getBean("sessionsCalculator");
			SettingsChangesProcessor settingsChanges = (SettingsChangesProcessor)this.appContext.getBean("settingsChangesProcessor");
			ApplicationWorker appWorker = (ApplicationWorker)this.appContext.getBean("applicationWorker");
			ScenariosGenerator scenariosGenerator = (ScenariosGenerator)this.appContext.getBean("scenariosGenerator");
			InputDataRequirementsRecognizer inputDataRecognizer = (InputDataRequirementsRecognizer)this.appContext.getBean("inputDataRequirementsRecognizer");
			SuitesWorker suitesWorker = (SuitesWorker)this.appContext.getBean("suitesWorker");
			ScenariosWorker scenariosWorker = (ScenariosWorker)this.appContext.getBean("scenariosWorker");

			switch (workType) {
			case PromoteForSessionCalculation:
				PromoteForSessionCalculationMessage sessionCalculation = MAPPER.readValue(request.getInputStream(),
						PromoteForSessionCalculationMessage.class);
				promoteForSessionCalculation.promoteForSessionCalculation(sessionCalculation);
				break;
			case CalculateSessions:
				CalculateSessionsMessage userReturned = MAPPER.readValue(request.getInputStream(),
						CalculateSessionsMessage.class);
				sessionsCalculator.calculateSessions(userReturned);
				break;
			case NewSession:
				UserSessionMessage userSession = MAPPER.readValue(request.getInputStream(), UserSessionMessage.class);
				graphTask.proccessUserSession(userSession);
				break;
			case ApplySettingsChanges:
				ChangedSettingsMessage changedSettings = MAPPER.readValue(request.getInputStream(), ChangedSettingsMessage.class);
				settingsChanges.processChanges(changedSettings);
				break;
			case CreateApplication:
				CreateApplicationMessage createApplication = MAPPER.readValue(request.getInputStream(), CreateApplicationMessage.class);
				appWorker.createApplication(createApplication);
				break;
			case GenerateScenarios:
				GenerateScenariosMessage generateScenarios = MAPPER.readValue(request.getInputStream(), GenerateScenariosMessage.class);
				scenariosGenerator.generateTestCases(generateScenarios);
				break;
			case RecognizeInputDataRequirements:
				RecognizeInputDataRequirementsMessage recognizeInputData = MAPPER.readValue(request.getInputStream(), RecognizeInputDataRequirementsMessage.class);
				inputDataRecognizer.recognizeInputDataRequirements(recognizeInputData);
				break;
			case DeleteSuite:
				DeleteSuiteMessage deleteSuite = MAPPER.readValue(request.getInputStream(), DeleteSuiteMessage.class);
				suitesWorker.deleteSuite(deleteSuite);
				break;
			case DeleteScenario:
				DeleteScenarioMessage deleteScenario = MAPPER.readValue(request.getInputStream(), DeleteScenarioMessage.class);
				scenariosWorker.deleteScenario(deleteScenario);
			case DeleteApplication:
				DeleteApplicationMessage deleteApplication = MAPPER.readValue(request.getInputStream(), DeleteApplicationMessage.class);
				appWorker.deleteApp(deleteApplication);
			default:
				break;
			}

			// Signal to beanstalk that processing was successful so this work
			// item should not be retried.
			response.setStatus(200);

		} catch (RuntimeException exception) {
			String stackTrace = getStackTrace(exception);
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.severe(stackTrace);

			// Signal to beanstalk that something went wrong while processing
			// the request. The work request will be retried several times in
			// case the failure was transient (eg a temporary network issue
			// when writing to Amazon S3).
			response.setStatus(500);

			try (PrintWriter writer = new PrintWriter(response.getOutputStream())) {
				exception.printStackTrace(writer);
			}
		}
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		response.setStatus(200);
		response.getWriter().write("Worker is working");
	}

	private void startScheduledTasks() {
		int delay = this.settings.get("promoteForSessionCalculationTaskDelay", 30);
		
		PromoteForSessionCalculationTask sessionCalculation = (PromoteForSessionCalculationTask)this.appContext.getBean("promoteForSessionCalculationTask");
		TaskScheduler taskScheduler = (TaskScheduler)this.appContext.getBean("taskScheduler");
		taskScheduler.scheduleWithFixedDelay("promoteForSessionCalculationTask", sessionCalculation, 0, delay, TimeUnit.SECONDS);
	}
	
	public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
}
