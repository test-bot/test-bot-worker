package com.usabilitics.testbot;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@WebListener
public class ContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		 AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		 ctx.scan("com.usabilitics.testbot");
		 ctx.refresh();

		sce.getServletContext().setAttribute("applicationContext", ctx);
	}

}
