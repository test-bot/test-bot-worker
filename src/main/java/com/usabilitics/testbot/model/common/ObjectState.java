package com.usabilitics.testbot.model.common;

public enum ObjectState {
	Pending,
	Processing,
	Ready
}
