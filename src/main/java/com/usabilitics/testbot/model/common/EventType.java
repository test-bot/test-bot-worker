package com.usabilitics.testbot.model.common;

public enum EventType {
	Click,
	EnterPressed,
	EscPressed,
	Focus,
	Visited
}
