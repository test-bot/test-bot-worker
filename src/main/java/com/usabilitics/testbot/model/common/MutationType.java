package com.usabilitics.testbot.model.common;

public enum MutationType {
	Added,
	Removed
}
