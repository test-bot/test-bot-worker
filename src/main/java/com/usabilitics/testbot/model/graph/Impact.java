package com.usabilitics.testbot.model.graph;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@RelationshipEntity(type="IMPACTS")
public class Impact extends Relation {
	public Impact() {
		this.scenariosIds = new ArrayList<>();
	}
	
	public Activity getCause() {
		return Cause;
	}

	public void setCause(Activity cause) {
		Cause = cause;
	}

	public Activity getEffect() {
		return Effect;
	}

	public void setEffect(Activity effect) {
		Effect = effect;
	}

	/**
	 * @return the hits - how many time this impact occurred.
	 */
	public long getHits() {
		return hits;
	}

	/**
	 * @param hits the hits to set - how many time this impact occurred.
	 */
	public void setHits(long hits) {
		this.hits = hits;
	}

	/**
	 * @return the pathId - a path is a collection of actions performed in many sessions in the same order
	 */
	public String getPathId() {
		return pathId;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @param pathId the pathId to set - a path is a collection of actions performed in many sessions in the same order
	 */
	public void setPathId(String pathId) {
		this.pathId = pathId;
	}

	/**
	 * @return the previousPathId - the id of the previous path if this impact is a start of a new path
	 */
	public String getPreviousPathId() {
		return previousPathId;
	}

	/**
	 * @param previousPathId - the id of the previous path if this impact is a start of a new path
	 */
	public void setPreviousPathId(String previousPathId) {
		this.previousPathId = previousPathId;
	}

	/**
	 * @return the isAfterSessionStart - whether this impact occurs after a session start node
	 */
	@JsonProperty(value="isAfterSessionStart")
	public boolean isAfterSessionStart() {
		return isAfterSessionStart;
	}

	/**
	 * @param isAfterSessionStart - whether this impact occurs after a session start node
	 */
	@JsonProperty(value="isAfterSessionStart")
	public void setAfterSessionStart(boolean isAfterSessionStart) {
		this.isAfterSessionStart = isAfterSessionStart;
	}

	/**
	 * @return the cycleNumber - the number of cycles that occurred before this impact
	 */
	public int getCyclesNumber() {
		return cyclesNumber;
	}

	/**
	 * @param cyclesNumber - the number of cycles that occurred before this impact
	 */
	public void setCyclesNumber(int cyclesNumber) {
		this.cyclesNumber = cyclesNumber;
	}

	/**
	 * @return the isPathStart - whether this impact is a beginning of a new sessions path
	 */
	@JsonProperty(value="isPathStart")
	public boolean isPathStart() {
		return isPathStart;
	}

	/**
	 * @param isPathStart - whether this impact is a beginning of a new sessions path
	 */
	@JsonProperty(value="isPathStart")
	public void setPathStart(boolean isPathStart) {
		this.isPathStart = isPathStart;
	}

	/**
	 * @return the isCycleStart - whether this impact is caused by repeated activity
	 */
	@JsonProperty(value="isCycleStart")
	public boolean isCycleStart() {
		return isCycleStart;
	}

	/**
	 * @param isCycleStart - whether this impact is caused by repeated activity
	 */
	@JsonProperty(value="isCycleStart")
	public void setCycleStart(boolean isCycleStart) {
		this.isCycleStart = isCycleStart;
	}

	/**
	 * @return the isCycleEnd - whether this impact is leading to a repeated activity
	 */
	@JsonProperty(value="isCycleEnd")
	public boolean isCycleEnd() {
		return isCycleEnd;
	}

	/**
	 * @param isCycleEnd - whether this impact is leading to a repeated activity
	 */
	@JsonProperty(value="isCycleEnd")
	public void setCycleEnd(boolean isCycleEnd) {
		this.isCycleEnd = isCycleEnd;
	}

	/**
	 * @return the pathOrdinal
	 */
	public int getPathOrdinal() {
		return pathOrdinal;
	}

	/**
	 * @param pathOrdinal the pathOrdinal to set
	 */
	public void setPathOrdinal(int pathOrdinal) {
		this.pathOrdinal = pathOrdinal;
	}

	/**
	 * @return the generatedScenarioId - if the impact diverges from a previous path into a new one, it is starting a new scenario.
	 */
	public String getGeneratedScenarioId() {
		return generatedScenarioId;
	}

	/**
	 * @param generatedScenarioId - if the impact diverges from a previous path into a new one, it is starting a new scenario.
	 */
	public void setGeneratedScenarioId(String generatedScenarioId) {
		this.generatedScenarioId = generatedScenarioId;
	}

	/**
	 * @return the scenariosIds - all ids of scenarios that passes through this impact.
	 */
	public List<String> getScenariosIds() {
		return scenariosIds;
	}

	/**
	 * @param scenariosIds - all ids of scenarios that passes through this impact.
	 */
	public void setScenariosIds(List<String> scenariosIds) {
		this.scenariosIds = scenariosIds;
	}

	@StartNode
	private Activity Cause;
	
	@EndNode
	private Activity Effect;
	
	private long hits;
	private String appId;
	private String pathId;
	private String previousPathId;
	private boolean isAfterSessionStart;
	private boolean isPathStart;
	private boolean isCycleStart;
	private boolean isCycleEnd;
	private int cyclesNumber;
	private int pathOrdinal;
	private String generatedScenarioId;
	private List<String> scenariosIds;
}
