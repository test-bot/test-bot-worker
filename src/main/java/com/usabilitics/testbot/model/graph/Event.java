package com.usabilitics.testbot.model.graph;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.typeconversion.EnumString;

import com.usabilitics.testbot.model.common.*;

@NodeEntity(label="Event")
public class Event extends Activity {
	@EnumString(value = EventType.class)
	private EventType type;

	private String targetOuterHTML;
	private boolean checkedForDataRequirement;
	private String inputType;
	private String targetLocalName;

	public Event() {
		super();
	}
	
	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	/**
	 * @return the targetOuterHTML
	 */
	public String getTargetOuterHTML() {
		return targetOuterHTML;
	}

	/**
	 * @param targetOuterHTML the targetOuterHTML to set
	 */
	public void setTargetOuterHTML(String targetOuterHTML) {
		this.targetOuterHTML = targetOuterHTML;
	}

	/**
	 * @return the checkedForDataRequirement
	 */
	public boolean isCheckedForDataRequirement() {
		return checkedForDataRequirement;
	}

	/**
	 * @param checkedForDataRequirement the checkedForDataRequirement to set
	 */
	public void setCheckedForDataRequirement(boolean checkedForDataRequirement) {
		this.checkedForDataRequirement = checkedForDataRequirement;
	}

	/**
	 * @return the inputType
	 */
	public String getInputType() {
		return inputType;
	}

	/**
	 * @param inputType the inputType to set
	 */
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	/**
	 * @return the targetLocalName
	 */
	public String getTargetLocalName() {
		return targetLocalName;
	}

	/**
	 * @param targetLocalName the targetLocalName to set
	 */
	public void setTargetLocalName(String targetLocalName) {
		this.targetLocalName = targetLocalName;
	}
}
