package com.usabilitics.testbot.model.graph;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Entity {

	Entity() {
		this.domainId = UUID.randomUUID().toString();
	}

    @JsonProperty("id")
    private Long id;

    private String domainId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
	 * @return the domainId - used to identify the object in the business layer and to compare it with other objects.
	 * Do not use the id field, it is for the graph's internal logic.
	 */
	public String getDomainId() {
		return domainId;
	}

	/**
	 * @param domainId the domainId to set - used to identify the object in the business layer and to compare it with other objects.
	 * Do not use the id field, it is for the graph's internal logic.
	 */
	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || domainId == null || getClass() != o.getClass()) return false;

        Entity entity = (Entity) o;

        if (!domainId.equals(entity.domainId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (domainId == null) ? -1 : domainId.hashCode();
    }
}