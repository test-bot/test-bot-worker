package com.usabilitics.testbot.model.graph;

public enum ActivityType {
	Mutation,
	Event
}
