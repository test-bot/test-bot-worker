package com.usabilitics.testbot.model.graph;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.typeconversion.EnumString;

import com.usabilitics.testbot.model.common.*;

@NodeEntity(label="Mutation")
public class Mutation extends Activity {
	@EnumString(value = MutationType.class)
	private MutationType type;
	private String mutatedElementSelector;
	
	public Mutation(){
		super();
	}
	
	public MutationType getType() {
		return type;
	}
	public void setType(MutationType type) {
		this.type = type;
	}
	
	/*
	 * Get the selector of the element that is mutated, for example the added or removed element.
	 */
	public String getMutatedElementSelector() {
		return mutatedElementSelector;
	}
	/*
	 * Get the selector of the element that is mutated, for example the added or removed element.
	 */
	public void setMutatedElementSelector(String mutatedElementSelector) {
		this.mutatedElementSelector = mutatedElementSelector;
	}
}
