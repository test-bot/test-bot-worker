package com.usabilitics.testbot.model.graph;

import java.util.*;

import org.neo4j.ogm.annotation.*;

/*
 * Represents activity on the application - user triggered event, site mutation etc.
 */
@NodeEntity(label="Activity")
public class Activity extends Entity {
	private String targetSelector;
	private int sessionStartHits;
	private String appId;
	private String pageUrl;
	
	@Relationship(type="IMPACTS",  direction= Relationship.OUTGOING)
	private Set<Impact> impacts;

	Activity() {
		super();
		this.impacts = new HashSet<Impact>();
	}
	
	public String getUniqueNodeIdentifier() {
		return this.getTargetSelector();
	}
	
	public String getTargetSelector() {
		return targetSelector;
	}
	public void setTargetSelector(String targetSelector) {
		this.targetSelector = targetSelector;
	}

	/**
	 * @return the sessionStartHits - how many times this node has been first in the user session.
	 */
	public int getSessionStartHits() {
		return sessionStartHits;
	}

	/**
	 * @param sessionStartHits the sessionStartHits to set - how many times this node has been first in the user session.
	 */
	public void setSessionStartHits(int sessionStartHits) {
		this.sessionStartHits = sessionStartHits;
	}
	
	/**
	 * @return the appId - identifier of the application to whom this node belongs
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId - identifier of the application to whom this node belongs
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public Set<Impact> getImpacts() {
		return impacts;
	}

	public void setImpacts(Set<Impact> impacts) {
		this.impacts = impacts;
	}

	/**
	 * @return the pageUrl
	 */
	public String getPageUrl() {
		return pageUrl;
	}

	/**
	 * @param pageUrl the pageUrl to set
	 */
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
}
