package com.usabilitics.testbot.connectivity.graph;

import javax.inject.Named;

import org.neo4j.ogm.service.Components;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

@Named
public class GraphSessionFactory {
	private SessionFactory sessionFactory;

    private String graphUrl;
    private Session session;

    public GraphSessionFactory() {
        Components.configuration()
    			.driverConfiguration()
	            .setDriverClassName("org.neo4j.ogm.drivers.http.driver.HttpDriver")
	            .setURI(this.getUrl());

    	this.sessionFactory = new SessionFactory("com.usabilitics.testbot.model.graph");
    	this.session = sessionFactory.openSession();
    }

    public Session getSession() {
        return this.session;
    }

    private String getUrl(){
    	if(this.graphUrl == null) {
    		String graphUrl = System.getProperty("GRAPHURL");
    		if (graphUrl == null) {
    			graphUrl = System.getenv("GRAPHURL");
    		}
    		if (graphUrl == null) {
    			throw new IllegalArgumentException("The property or system environment GRAPHURL is not specified.");
    		}

    		this.graphUrl = graphUrl;
    	}

    	return this.graphUrl;
    }
}
