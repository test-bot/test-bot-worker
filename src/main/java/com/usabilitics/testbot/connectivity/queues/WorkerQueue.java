package com.usabilitics.testbot.connectivity.queues;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.gson.Gson;

@Named
public class WorkerQueue {
	public WorkerQueue() {
		this.sqs = new AmazonSQSClient();
		 Region r = Region.getRegion(Regions.US_WEST_1);
	        sqs.setRegion(r);	
	}

	public <T> void sendMessage(T message, Map<String, String> messageAttributes) {
		// Serialize message body.
		Gson gson = new Gson();
		String serializedMessage = gson.toJson(message);
		
		// Prepare message attributes.
		Map<String, MessageAttributeValue> messageAtrrbutesValues = new HashMap<String, MessageAttributeValue>();

		for (Map.Entry<String, String> entry : messageAttributes.entrySet()) {
			MessageAttributeValue value = new MessageAttributeValue().withDataType("String").withStringValue(entry.getValue());
			messageAtrrbutesValues.put(entry.getKey(), value);
		}

		SendMessageRequest request = new SendMessageRequest();
		request.withQueueUrl(this.getUrl());
		request.withMessageAttributes(messageAtrrbutesValues);
		request.withMessageBody(serializedMessage);
		this.sqs.sendMessage(request);
	}
	
	private String getUrl(){
		if(this.workerQueueUrl == null) {
			String queueUrl = System.getProperty("QUEUE_URL");
			if (queueUrl == null) {
				queueUrl = System.getenv("QUEUE_URL");
			}
			if (queueUrl == null) {
				throw new IllegalArgumentException("The property or system environment QUEUE_URL is not specified.");
			}

			this.workerQueueUrl = queueUrl;
		}
		
		return this.workerQueueUrl;
	}

	private AmazonSQS sqs;
	private String workerQueueUrl;
}
