package com.usabilitics.testbot.connectivity.database;

import javax.inject.Named;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Named
public class DatabaseClient {	
	private String databaseUrl;
	private MongoClient client;
	
	public DatabaseClient(){
		MongoClientURI connectionString = new MongoClientURI(this.getUrl());
		this.client = new MongoClient(connectionString);
	}

	public MongoClient getClient(){
		return this.client;
	}
	
	private String getUrl(){
		if(this.databaseUrl == null) {
			String databaseUrl = System.getProperty("DBURL");
			if (databaseUrl == null) {
				databaseUrl = System.getenv("DBURL");
			}
	
			if (databaseUrl == null) {
				throw new IllegalArgumentException("The property or system environment DBURL is not specified.");
			}
			if (databaseUrl.indexOf("mongodb://") != 0) {
				databaseUrl = "mongodb://" + databaseUrl;
			}
			
			this.databaseUrl = databaseUrl;
		}
		
		return this.databaseUrl;
	}
}
