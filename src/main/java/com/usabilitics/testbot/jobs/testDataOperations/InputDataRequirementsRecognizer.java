package com.usabilitics.testbot.jobs.testDataOperations;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.setOnInsert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.RecognizeInputDataRequirementsMessage;
import com.usabilitics.testbot.model.common.EventType;
import com.usabilitics.testbot.model.graph.Event;

@Named
public class InputDataRequirementsRecognizer {
	private Session graphSession;
	private MongoDatabase database;
	private MongoCollection<Document> nodesInfoCollection;

	@Inject
	public InputDataRequirementsRecognizer(DatabaseClient dbClient, GraphSessionFactory graphFactory) {
		this.graphSession = graphFactory.getSession();

		this.database = dbClient.getClient().getDatabase("test-bot");
		this.nodesInfoCollection = this.database.getCollection("nodesInfo");
	}

	public void recognizeInputDataRequirements(RecognizeInputDataRequirementsMessage message) {
		Logger logger = Logger.getLogger("jobs");
		logger.log(Level.FINER, "RecognizeInputDataRequirements task started:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));

		Iterable<Event> events = this.getFocusNodes(message.getAppId());

		for (Event event : events) {
			Bson doc = this.createUpdateDocument(event, message.getAccountId());
			this.nodesInfoCollection.updateOne(
					eq("nodeId", event.getDomainId()),
					doc,
					new UpdateOptions().upsert(true));

			event.setCheckedForDataRequirement(true);
			this.graphSession.save(event);
		}

		logger.log(Level.FINER, "RecognizeInputDataRequirements task ended:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
	}
	
	private Iterable<Event> getFocusNodes (String appId) {
		String query = "MATCH (e:Event) WHERE e.appId={appId} " +
				"AND " +
						"(e.type={type} " +
					"OR " +
						"(e.targetLocalName={localName} AND e.type={enter})) " +
				"AND NOT(e.inputType = {checkbox}) " +
				"AND NOT(e.inputType = {radio}) " +
				"AND NOT(e.inputType = {submit}) " +
				"AND NOT(e.inputType = {button}) " +
				"AND NOT(e.inputType = {image}) " +
				"AND e.checkedForDataRequirement = {checkedForDataRequirement} " +
				"RETURN e";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("appId", appId);
		params.put("type", EventType.Focus);
		params.put("checkbox", "checkbox");
		params.put("radio", "radio");
		params.put("submit", "submit");
		params.put("button", "button");
		params.put("image", "image");
		params.put("localName", "input");
		params.put("enter", EventType.EnterPressed);
		params.put("checkedForDataRequirement", false);

		return this.graphSession.query(Event.class, query, params);
	}

	private Bson createUpdateDocument (Event event, String accountId) {
		Document acl = new Document();

		ArrayList<String> list = new ArrayList<String>();
		list.add(accountId);
		acl.append("canRead", list);
		acl.append("canUpdate", list);

		Bson update = combine(
				set("appId", event.getAppId()),
				set("targetOuterHTML", event.getTargetOuterHTML()),
				set("targetSelector", event.getTargetSelector()),
				set("targetLocalName", event.getTargetLocalName()),
				set("inputType", event.getInputType()),
				set("type", event.getType().toString()),
				set("requiresData", true),
				set("checkedForDataRequirement", true),
				setOnInsert("acl", acl));

		return update;
	}
}
