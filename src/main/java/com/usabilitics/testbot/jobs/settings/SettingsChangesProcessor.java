package com.usabilitics.testbot.jobs.settings;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import com.usabilitics.testbot.jobs.TaskScheduler;
import com.usabilitics.testbot.jobs.sessionsCalculator.PromoteForSessionCalculationTask;
import com.usabilitics.testbot.messages.ChangedSettingsMessage;
import com.usabilitics.testbot.settings.Settings;

@Named
public class SettingsChangesProcessor {
	private Settings settings;
	private TaskScheduler scheduler;
	private PromoteForSessionCalculationTask promoteForSessionCalculationTask;

	@Inject
	public SettingsChangesProcessor(Settings settings, TaskScheduler scheduler, PromoteForSessionCalculationTask sessionCalcTask) {
		this.settings = settings;
		this.scheduler = scheduler;
		this.promoteForSessionCalculationTask = sessionCalcTask;
	}
	
	public void processChanges(ChangedSettingsMessage settings) {
		for (String key : settings.getKeys()) {
			switch(key) {
			case "promoteForSessionCalculationTaskDelay":
				this.resetPromoteForSessionCalculationTask();
				break;
			}
		}
	}
	
	private void resetPromoteForSessionCalculationTask() {
		scheduler.resetTask("promoteForSessionCalculationTask");
		
		int delay = settings.get("promoteForSessionCalculationTaskDelay", 30);

		this.scheduler.scheduleWithFixedDelay(
				"promoteForSessionCalculationTask",
				this.promoteForSessionCalculationTask, 0, delay, TimeUnit.SECONDS);
	}
}
