package com.usabilitics.testbot.jobs.sessionsCalculator;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.queues.WorkerQueue;
import com.usabilitics.testbot.messages.CalculateSessionsMessage;
import com.usabilitics.testbot.messages.PromoteForSessionCalculationMessage;

@Named
public class PromoteForSessionCalculation {
	@Inject
	public PromoteForSessionCalculation(DatabaseClient database, WorkerQueue queue) {
		super();
		this.database = database.getClient().getDatabase("test-bot");
		this.sentencesCollection = this.database.getCollection("sentences");
		this.queue = queue;
	}

	public void promoteForSessionCalculation(PromoteForSessionCalculationMessage promoteForSessionCalculationMessage) {
		Logger logger = Logger.getLogger("jobs");
		logger.log(Level.FINER, "PromoteUsersForSessionCalculation task started:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
		
		MongoCollection<Document> appsCollection = this.database.getCollection("applications");
		Document app = appsCollection.find(new Document("id", promoteForSessionCalculationMessage.getAppId())).first();

		// Get the last activity dates for all users.
		String map = 
				"function() {"
					+ "emit(this.userId, this.timestamp);"
				+ "}";
		String reduce =
				"function(userId, activityTimestamps){"
					+ "return activityTimestamps.sort()[activityTimestamps.length - 1];"
				+ "}";

		MongoCursor<Document> timestampsIterator = this.sentencesCollection.mapReduce(map, reduce)
			.filter(and(
						eq("appId", app.getString("id")),
						eq("processed", false),
						eq("activityType", "Event")))
			.iterator();

		int expirationValue = app.get("settings", Document.class).getInteger("sessionSlidingExpiration");

		Period sessionSlidingExpiration = Period.seconds(expirationValue);

		// Send tasks to the queue for each user that hasn't been active since given time period.
		while (timestampsIterator.hasNext()) {
			Document userLastActivityTimeStamp = timestampsIterator.next();

			DateTime now = DateTime.now();
			DateTime activityDate = new DateTime(userLastActivityTimeStamp.getDouble("value").longValue());
			Interval passedTimeFromLastActivity = new Interval(activityDate, now);

			if(passedTimeFromLastActivity.toDuration().compareTo(sessionSlidingExpiration.toDurationTo(now)) > 0) {
				String userId = userLastActivityTimeStamp.getString("_id");

				// Add task to the queue with the specific user and app id that needs sessions calculation.
				CalculateSessionsMessage message = new CalculateSessionsMessage(app.getString("id"), userId);
				Map<String, String> attributes = new HashMap<String, String>();
				attributes.put("WorkType", "CalculateSessions");

				this.queue.sendMessage(message, attributes);
			}
		}
		
		logger.log(Level.FINER, "PromoteUsersForSessionCalculation task ended:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
	}
	
	private MongoDatabase database;
	private MongoCollection<Document> sentencesCollection;
	private WorkerQueue queue;
}
