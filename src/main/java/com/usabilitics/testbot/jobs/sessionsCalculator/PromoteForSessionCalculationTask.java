package com.usabilitics.testbot.jobs.sessionsCalculator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.queues.*;
import com.usabilitics.testbot.messages.*;
import com.usabilitics.testbot.model.common.ObjectState;
import com.usabilitics.testbot.settings.Settings;

import static com.mongodb.client.model.Filters.*;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

@Named
public class PromoteForSessionCalculationTask implements Runnable {

	@Inject
	public PromoteForSessionCalculationTask(DatabaseClient database, WorkerQueue queue) {
		this.database = database.getClient().getDatabase("test-bot");
		this.applicationsCollection = this.database.getCollection("applications");
		this.queue = queue;
	}

	@Override
	public void run() {
		try {
			FindIterable<Document> apps = this.applicationsCollection.find(new Document("state", ObjectState.Ready.name()));
			
			for (Document app : apps) {
				PromoteForSessionCalculationMessage message = new PromoteForSessionCalculationMessage(app.getString("id"));
				Map<String, String> attributes = new HashMap<String, String>();
				attributes.put("WorkType", "PromoteForSessionCalculation");
	
				this.queue.sendMessage(message, attributes);
			}
		}
		catch (RuntimeException exception) {
			// TODO: extract logging in a shared place
			String stackTrace = getStackTrace(exception);
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.severe(stackTrace);
		}
	}
	
	public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}

	private MongoDatabase database;
	private MongoCollection<Document> applicationsCollection;
	private WorkerQueue queue;
}
