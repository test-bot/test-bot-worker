package com.usabilitics.testbot.jobs.sessionsCalculator;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.queues.WorkerQueue;
import com.usabilitics.testbot.messages.CalculateSessionsMessage;
import com.usabilitics.testbot.messages.UserSessionMessage;
import com.usabilitics.testbot.settings.Settings;

import static com.mongodb.client.model.Filters.*;

@Named
public class SessionsCalculator {
	@Inject
	public SessionsCalculator(DatabaseClient database, Settings settings, WorkerQueue queue) {
		this.database = database.getClient().getDatabase("test-bot");
		this.sentencesCollection = this.database.getCollection("sentences");
		this.settings = settings;
		this.queue = queue;
	}
	
	public void calculateSessions(CalculateSessionsMessage message) {
		Logger logger = Logger.getLogger("jobs");
		logger.log(Level.FINER, "CalculateSessions task started:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));

		List<UserSession> sessions = this.calculateSessions(message.getAppId(), message.getUserId());
		
		for (UserSession userSession : sessions) {
			UserSessionMessage m = new UserSessionMessage(userSession);
			m.setUserId(message.getUserId()); 
			m.setAppId(message.getAppId());

			Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("WorkType", "NewSession");

			this.queue.sendMessage(m, attributes);
		}
		
		logger.log(Level.FINER, "CalculateSessions task ended:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
	}
	
	public List<UserSession> calculateSessions(String appId, String userId) {
		// Attempt to use Kernel Density Estimator. In this case we have to find the local minimals and use them as session boundaries.
//		FindIterable<Document> sentences = this.sentencesCollection.find(
//				and(
//						eq("userId", message.getUserId()),
//						eq("processed", false)));
//
//		double bandwidth = 50;
//		
//		KernelEstimator estimator = new KernelEstimator(bandwidth);
//		
//		sentences.forEach(new Block<Document> () {
//			@Override
//			public void apply (final Document s) {
//				double timestamp = 0;
//				try {
//					timestamp = s.getDouble("timestamp"); 
//				}
//				catch(ClassCastException ex) {
//					try {
//						timestamp = (double)s.getInteger("timestamp");
//					}
//					catch (ClassCastException e) {
//					}
//				}
//
//				estimator.addValue(timestamp, 1);
//			}
//		});


		FindIterable<Document> sentences = this.sentencesCollection.find(
				and(
					eq("appId", appId),
					eq("userId", userId),
					eq("processed", false),
					eq("activityType", "Event")))
				.sort(new Document("timestamp", 1))
				.projection(new Document("timestamp", 1));

		int expirationValue = this.settings.get("sessionSlidingExpration", 30);

		MongoCursor<Document> sentencesCursor = sentences.iterator();

		ArrayList<UserSession> sessions = new ArrayList<UserSession>();

		if(!sentencesCursor.hasNext()) {
			return new ArrayList<UserSession>();
		}

		long currentLeftBoundary;
		long currentRightBoundary;
		Document current = sentencesCursor.next();

		currentLeftBoundary = this.getTimestampInMs(current);

		while (current != null) {
			long currentTimestamp = this.getTimestampInMs(current);

			if(sentencesCursor.hasNext()) {
				Document next = sentencesCursor.next();
				long nextTimestamp = this.getTimestampInMs(next);

				if(nextTimestamp - currentTimestamp > expirationValue * 1000L) {
					currentRightBoundary = currentTimestamp;
					sessions.add(new UserSession(currentLeftBoundary, currentRightBoundary));
					currentLeftBoundary = nextTimestamp;
				}

				current = next;
			}
			else {
				currentRightBoundary = currentTimestamp;
				sessions.add(new UserSession(currentLeftBoundary, currentRightBoundary));
				current = null;
			}
		}

		return sessions;
	}
	
	private long getTimestampInMs(Document document) {
		try {
			return document.getDouble("timestamp").longValue(); 
		}
		catch(ClassCastException ex) {
			try {
				return document.getInteger("timestamp").longValue();
			}
			catch (ClassCastException e) {
			}
		}
		
		return 0;
	}
	
	private MongoDatabase database;
	private MongoCollection<Document> sentencesCollection;
	private Settings settings;
	private WorkerQueue queue;
}
