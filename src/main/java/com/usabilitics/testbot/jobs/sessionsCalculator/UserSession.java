package com.usabilitics.testbot.jobs.sessionsCalculator;

public class UserSession {
	private long startDate;
	private long endDate;
	
	public UserSession(long startDate, long endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
}
