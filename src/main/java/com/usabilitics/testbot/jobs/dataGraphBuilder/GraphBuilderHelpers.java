package com.usabilitics.testbot.jobs.dataGraphBuilder;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lte;

import javax.inject.Named;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;

import com.usabilitics.testbot.messages.UserSessionMessage;
import com.usabilitics.testbot.model.common.EventType;
import com.usabilitics.testbot.model.common.MutationType;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.ActivityType;
import com.usabilitics.testbot.model.graph.Event;
import com.usabilitics.testbot.model.graph.Mutation;

@Named
public class GraphBuilderHelpers {

	public Filters getNodeFilters(Document sentence) {
		Filters filters = new Filters();
		filters.add("targetSelector", sentence.get("targetSelector"));

		Filter typeFilter = new Filter("type", sentence.getString("type"));
		typeFilter.setBooleanOperator(BooleanOperator.AND);
		filters.add(typeFilter);

		Filter appFilter = new Filter("appId", sentence.getString("appId"));
		appFilter.setBooleanOperator(BooleanOperator.AND);
		filters.add(appFilter);

		Filter pageFilter = new Filter("pageUrl", sentence.getString("pageUrl"));
		pageFilter.setBooleanOperator(BooleanOperator.AND);
		filters.add(pageFilter);

		ActivityType activityType = ActivityType.valueOf(sentence.getString("activityType"));
		switch (activityType) {
			case Mutation:
				Filter selectorFilter = new Filter("mutatedElementSelector", sentence.getString("mutatedElementSelector"));
				selectorFilter.setBooleanOperator(BooleanOperator.AND);
				filters.add(selectorFilter);
				break;
			case Event:
				break;
			default:
				break;
		}
		return filters;
	}

	public Activity createNodeFromSentence(Document sentence) {
		ActivityType activityType = ActivityType.valueOf(sentence.getString("activityType"));
		Activity activity = null;
		switch (activityType) {
		case Event:
			Event event = new Event();
			event.setType(EventType.valueOf(sentence.getString("type")));
			event.setTargetLocalName(sentence.getString("targetLocalName"));
			event.setTargetOuterHTML(sentence.getString("outerHTML"));
			event.setInputType(sentence.getString("inputType"));
			activity = event;
			break;
		case Mutation:
			Mutation mutation = new Mutation();
			mutation.setMutatedElementSelector(sentence.getString("mutatedElementSelector"));
			mutation.setType(MutationType.valueOf(sentence.getString("type")));
			activity = mutation;
			break;
		default:
			break;
		}

		activity.setAppId(sentence.getString("appId"));
		activity.setTargetSelector(sentence.getString("targetSelector"));
		activity.setPageUrl(sentence.getString("pageUrl"));

		return activity;
	}
	
	public Class<?> getNodeClass(ActivityType activityType) {
		Class<?> nodeClass;
		
		switch (activityType) {
			case Event:
				nodeClass = Event.class;
				break;
			case Mutation:
				nodeClass = Mutation.class;
				break;
			default:
				nodeClass = Activity.class;
				break;
		}
		
		return nodeClass;
	}

	public Bson getSentencesFilter(UserSessionMessage userSession){
		Bson filter = 
			and(
				eq("userId", userSession.getUserId()),
				eq("appId", userSession.getAppId()),
				eq("activityType", ActivityType.Event.toString()),
				gte("timestamp", userSession.getStartDate()),
				lte("timestamp", userSession.getEndDate()));
		return filter;
	}
	
}
