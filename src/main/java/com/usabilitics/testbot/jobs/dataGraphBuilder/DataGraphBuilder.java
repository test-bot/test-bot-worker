package com.usabilitics.testbot.jobs.dataGraphBuilder;

import static com.mongodb.client.model.Filters.and;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.UserSessionMessage;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.ActivityType;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class DataGraphBuilder {
	@Inject
	public DataGraphBuilder(DatabaseClient dbClient, GraphSessionFactory graphFactory, GraphBuilderHelpers helper) {
		this.graphSession = graphFactory.getSession();
		this.helper = helper;
		
		this.database = dbClient.getClient().getDatabase("test-bot");
		this.sentencesCollection = this.database.getCollection("sentences");
	}

	public void proccessUserSession(UserSessionMessage userSession) {
		Logger logger = Logger.getLogger("jobs");
		logger.log(Level.FINER, "ProcessUserSession task started:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
		
		MongoCursor<Document> sentencesCursor = this.sentencesCollection
			.find(
				and(
					this.helper.getSentencesFilter(userSession),
					new Document("processed", new Document("$ne", true))))
			.sort(new Document("timestamp", 1))
			.iterator();

		Transaction tx = this.graphSession.beginTransaction();

		try {
			boolean isFirstNode = true;
			boolean isSecondNode = false;
			boolean hasDiverged = false;

			Activity previousNode = null;
			Activity currentNode = null;

			String currentPathId = null;

			int cyclesNumber = 0;

			int pathOrdinal = 0;

			HashMap<Activity, Integer> visitedNodes = new HashMap<Activity, Integer>();

		    while (sentencesCursor.hasNext()) {
		    	Document sentence = sentencesCursor.next();

		    	ActivityType activityType = ActivityType.valueOf(sentence.get("activityType").toString());
				
				Class<?> nodeClass = this.helper.getNodeClass(activityType);

				// Search for current node in the graph.
				Filters filters = this.helper.getNodeFilters(sentence);
				Collection<?> currentNodeCollection = this.graphSession.loadAll(nodeClass, filters);

				// Get or create the current node.
				if(currentNodeCollection.iterator().hasNext()) {
					currentNode = (Activity)currentNodeCollection.iterator().next();
					if(isFirstNode)
						currentNode.setSessionStartHits(currentNode.getSessionStartHits() + 1);
				}
				else {
					currentNode = this.helper.createNodeFromSentence(sentence);
					if(isFirstNode)
						currentNode.setSessionStartHits(1);
				}

				// A new node receives id when it is saved and the id is used as a hash key.
				this.graphSession.save(nodeClass.cast(currentNode), 1);

				// If the previous node has been visited more than once, we have a new cycle so the counter is incremented.
				boolean cycleStarted = false;
				if(previousNode != null &&
					visitedNodes.containsKey(previousNode) &&
					visitedNodes.get(previousNode) > 1) {
						cyclesNumber++;
						cycleStarted = true;
				}

				// If the current node is repeated, we have a closed cycle.
				boolean cycleEnded = false;
				Integer visits = visitedNodes.get(currentNode);
				if(visits == null || visits == 0) {
					visitedNodes.put(currentNode, 1);
				}
				else {
					visitedNodes.replace(currentNode, visits + 1);
					cycleEnded = true;
				}

				// If first continue with the next.
				if(isFirstNode) {
					isFirstNode = false;
					isSecondNode = true;
					previousNode = currentNode;
					continue;
				}

				// Find impact from the previous node.
				Iterator<Impact> impactsIter = previousNode.getImpacts().iterator();
				Impact impact = null;
				while(impactsIter.hasNext()){
					Impact currentImpact = impactsIter.next();
					if(currentImpact.getEffect().equals(currentNode)) {
						if(isSecondNode) {
							if(currentImpact.isAfterSessionStart()) {
								impact = currentImpact;
							}
						}
						else if (currentImpact.getPathId().equals(currentPathId) &&
								currentImpact.getCyclesNumber() == cyclesNumber) {
							impact = currentImpact;
						}
					}
				}

				// We check if there is a new path starting from this node
				if(impact == null) {
					impactsIter = previousNode.getImpacts().iterator();
					while(impactsIter.hasNext()){
						Impact currentImpact = impactsIter.next();
						if(currentImpact.getEffect().equals(currentNode) &&
								currentImpact.isPathStart() &&
								currentImpact.getPreviousPathId() != null &&
								currentImpact.getPreviousPathId().equals(currentPathId)) {
							impact = currentImpact;
						}
					}
				}

				// Update or create impact.
				if(impact == null) {
					impact = new Impact();
					impact.setCause(previousNode);
					impact.setEffect(currentNode);
					impact.setHits(1);
					impact.setCyclesNumber(cyclesNumber);
					impact.setCycleStart(cycleStarted);
					impact.setCycleEnd(cycleEnded);
					impact.setAppId(userSession.getAppId());
					impact.setPathOrdinal(pathOrdinal);

					pathOrdinal++;

					// A session diverges when there is no existing path to follow. After that a new path id is generated and it is used in all new impacts.
					if(hasDiverged) {
						impact.setPathId(currentPathId);
					}
					else {
						impact.setPathId(UUID.randomUUID().toString());
						impact.setPreviousPathId(currentPathId);
						impact.setPathStart(true);
						hasDiverged = true;
					}

					impact.setAfterSessionStart(isSecondNode);
					previousNode.getImpacts().add(impact);
				}
				else {
					impact.setHits(impact.getHits() + 1);
				}

				currentPathId = impact.getPathId();

				this.graphSession.save(impact, 1);
				this.graphSession.save(nodeClass.cast(currentNode), 1);

				previousNode = currentNode;

				if(!isFirstNode)
					isSecondNode = false;
		    }

		    tx.commit();

		    this.sentencesCollection.updateMany(
		    		this.helper.getSentencesFilter(userSession),
		    		new Document("$set", new Document("processed", true)));
		}
		catch (Exception ex) {
			tx.rollback();
			throw new RuntimeException("The data graph builder task encountered error when processing user session.", ex);
		}
		finally {
			sentencesCursor.close();
			logger.log(Level.FINER, "ProcessUserSession task ended:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
		}

		tx.close();
	}

	private MongoDatabase database;
	private MongoCollection<Document> sentencesCollection;
	private Session graphSession;
	private GraphBuilderHelpers helper;
}
