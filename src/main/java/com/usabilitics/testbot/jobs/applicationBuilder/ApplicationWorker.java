package com.usabilitics.testbot.jobs.applicationBuilder;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.neo4j.ogm.session.Session;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.CreateApplicationMessage;
import com.usabilitics.testbot.messages.DeleteApplicationMessage;
import com.usabilitics.testbot.model.common.ObjectState;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class ApplicationWorker {
	@Inject
	public ApplicationWorker(DatabaseClient database, GraphSessionFactory sessionFactory) {
		this.database = database.getClient().getDatabase("test-bot");
		this.session = sessionFactory.getSession();
		this.appsCollection = this.database.getCollection("applications");
	}

	public void createApplication(CreateApplicationMessage message){
		Document app = this.appsCollection.find(new Document("id", message.getAppId())).first();
		if(app != null && app.getString("state").equals(ObjectState.Pending.name())){
			// Update status
			this.appsCollection.findOneAndUpdate(
					new Document("id", message.getAppId()),
					new Document("$set", new Document("state", ObjectState.Processing.name())));
			
			// Set default values
			Document settings = new Document();
			settings.append("sessionSlidingExpiration", 30);
			
			// Update settings
			this.appsCollection.findOneAndUpdate(
					new Document("id", message.getAppId()),
					new Document("$set",
							new Document("state", ObjectState.Ready.name())
								 .append("settings", settings)));
		}
		
	}

	public void deleteApp(DeleteApplicationMessage message) {
		Document appIdFilter = new Document("appId", message.getAppId());

		MongoCollection<Document> sentences = this.database.getCollection("sentences");
		sentences.deleteMany(appIdFilter);

		MongoCollection<Document> executedSteps = this.database.getCollection("executedSteps");
		executedSteps.deleteMany(appIdFilter);

		MongoCollection<Document> scenarios = this.database.getCollection("scenarios");
		scenarios.deleteMany(appIdFilter);

		MongoCollection<Document> suites = this.database.getCollection("suites");
		suites.deleteMany(appIdFilter);

		MongoCollection<Document> testRuns = this.database.getCollection("testRuns");
		testRuns.deleteMany(appIdFilter);

		MongoCollection<Document> applications = this.database.getCollection("applications");
		applications.deleteOne(new Document("id", message.getAppId()));

		String deleteRelations = "MATCH ()-[a{appId: {appId}}]-() " +
							 "DELETE a";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("appId", message.getAppId());

		session.query(deleteRelations, params);

		String deleteNodes = "MATCH (a{appId: {appId}}) " +
				 "DELETE a";

		session.query(deleteNodes, params);
	}

	private MongoDatabase database;
	private MongoCollection<Document> appsCollection;
	private Session session;
}
