package com.usabilitics.testbot.jobs;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

import javax.inject.Named;

import org.neo4j.ogm.session.Session;

import com.mongodb.MongoClient;
import com.usabilitics.testbot.jobs.dataGraphBuilder.DataGraphBuilder;

/*
 * Starts the task on specified elapsed amount of time
 */
@Named
public class TaskScheduler {
	public TaskScheduler() {
		this.tasksFutures = new HashMap<String, ScheduledFuture<?>>();
	}

	public ScheduledExecutorService getSchedulerService() {
		return this.scheduler;
	}
	
	public void scheduleWithFixedDelay(String taskKey, Runnable task, long initialDelay, long delay, TimeUnit unit) {
		if(this.tasksFutures.containsKey(taskKey)) {
			return;
		}

		ScheduledFuture<?> future = this.scheduler.scheduleWithFixedDelay(task, initialDelay, delay, unit);
		this.tasksFutures.put(taskKey, future);
	}
	
	public void resetTask(String taskKey){
		this.tasksFutures.get(taskKey).cancel(true);
		this.tasksFutures.remove(taskKey);
	}
	
	public Map<String, ScheduledFuture<?>> getTasksFutures() {
		return tasksFutures;
	}

	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

	private Map<String, ScheduledFuture<?>> tasksFutures;
}
