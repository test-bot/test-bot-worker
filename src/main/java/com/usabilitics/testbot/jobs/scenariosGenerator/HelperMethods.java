package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.beans.PersistenceDelegate;
import java.util.Map;

import org.bson.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.usabilitics.testbot.model.graph.*;

public class HelperMethods {
	public static Activity mapToActivity(Map<String, Object> map) {
		Class<?> type;
		// HACK: determine the type of the activity based on this property
		if(map.containsKey("mutatedElementSelector")) {
			type = Mutation.class;
		}
		else {
			type = Event.class;
		}

		Activity result;
		try {
			result = (Activity) mapper.convertValue(map, type);
			return result;
		} catch (Exception e) {
			// It is not Activity.
			return null;
		}
	}

	public static Document createDocumentFromActivity(Object activity) {
		Document persistedActivity = new Document();
		if (activity instanceof Event) {
			Event event = (Event)activity;
			persistedActivity.append("nodeId", event.getDomainId());
			persistedActivity.append("activityType", "Event");
			persistedActivity.append("type", event.getType().toString());
			persistedActivity.append("targetSelector", event.getTargetSelector());
			persistedActivity.append("pageUrl", event.getPageUrl());
		}

		if (activity instanceof Mutation) {
			Mutation mutation = (Mutation)activity;
			persistedActivity.append("nodeId", mutation.getDomainId());
			persistedActivity.append("activityType", "Mutation");
			persistedActivity.append("type", mutation.getType().toString());
			persistedActivity.append("targetSelector", mutation.getTargetSelector());
			persistedActivity.append("mutatedElementSelector", mutation.getMutatedElementSelector());
			persistedActivity.append("pageUrl", mutation.getPageUrl());
		}
		
		return persistedActivity;
	}

	public final static ObjectMapper mapper = new ObjectMapper();
}


