package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.Collection;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.and;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.DeleteSuiteMessage;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class SuitesWorker {

	@Inject
	public SuitesWorker(GraphSessionFactory graphSessionFactory, DatabaseClient db) {
		this.database = db.getClient().getDatabase("test-bot");
		this.graphSession = graphSessionFactory.getSession();
	}
	
	public void deleteSuite(DeleteSuiteMessage message) {
		this.deleteSuiteFromGraph(message.getAppId());

		MongoCollection<Document> suites = this.database.getCollection("suites");

		suites.deleteOne(new Document("id", message.getSuiteId()));

		MongoCollection<Document> scenarios = this.database.getCollection("scenarios");
		scenarios.deleteMany(
				and(
						eq("appId", message.getAppId()),
						eq("suiteId", message.getSuiteId())));
	}
	
	private void deleteSuiteFromGraph(String appId) {
		Filters filters = new Filters();
		filters.add("appId", appId);

		Collection<Impact> impacts = this.graphSession.loadAll(Impact.class, filters);
		for (Impact impact : impacts) {
			impact.setScenariosIds(null);
			impact.setGeneratedScenarioId(null);
			this.graphSession.save(impact);
		}
	}
	
	private MongoDatabase database;
	private Session graphSession;
}
