package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.*;

import com.usabilitics.testbot.model.graph.Activity;

/*
 * Single path
 */
public class Path<T> implements Iterable<PathNode<T>> {

	public Path(T node) {
		this.setFirst(new PathNode<T>(node));
		this.setLast(new PathNode<T>(node));
		this.size = 1;
	}

	public Path() {
		// TODO Auto-generated constructor stub
	}

	public PathNode<T> add(T node) {
		PathNode<T> newNode = new PathNode<T>(node);

		newNode.setPrevious(this.getLast());

		if(this.getLast() != null) {
			this.getLast().setNext(newNode);
		}

		if(this.size == 0) {
			this.setFirst(newNode);
			if(this.getPrevious() != null) {
				this.getFirst().setPrevious(this.getPrevious());
			}
		}

		newNode.setOrdinal(this.lastElementOrdinal);
		this.lastElementOrdinal++;

		this.setLast(newNode);

		this.size++;

		return newNode;
	}

	public void add(Iterable<T> nodes) {
		for (T node : nodes) {
			this.add(node);
		}
	}

	public int getSize() {
		return this.size;
	}

	@Override
	public Iterator<PathNode<T>> iterator() {
		return new PathIterator<PathNode<T>>(this.first);
	}

	/**
	 * @return the first
	 */
	public PathNode<T> getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	private void setFirst(PathNode<T> first) {
		this.first = first;
	}

	/**
	 * @return the last
	 */
	public PathNode<T> getLast() {
		return last;
	}

	/**
	 * @param last the last to set
	 */
	private void setLast(PathNode<T> last) {
		this.last = last;
	}

	/**
	 * @return the pathId
	 */
	public String getPathId() {
		return pathId;
	}

	/**
	 * @param pathId the pathId to set
	 */
	public void setPathId(String pathId) {
		this.pathId = pathId;
	}

	/**
	 * @return the previous
	 */
	public PathNode<T> getPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(PathNode<T> previous) {
		this.previous = previous;
	}

	private PathNode<T> first;
	private PathNode<T> last;
	private int size;
	private String pathId;
	private PathNode<T> previous;
	private int lastElementOrdinal = 0;
}
