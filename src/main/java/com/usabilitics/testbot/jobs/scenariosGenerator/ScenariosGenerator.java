package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.neo4j.ogm.session.Session;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.GenerateScenariosMessage;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class ScenariosGenerator {
	@Inject
	public ScenariosGenerator(
			DatabaseClient database,
			GraphSessionFactory graphSessionFactory,
			GraphOperations graphOperations,
			ScenariosGraphRecorder recorder,
			ScenariosStorageRecorder storageRecorder) {
		this.graphSession = graphSessionFactory.getSession();
		this.database = database.getClient().getDatabase("test-bot");
		this.graphRecorder = recorder;
		this.graphOperations = graphOperations;
		this.storageRecorder = storageRecorder;
	}

	public void generateTestCases(GenerateScenariosMessage message) {
		this.generateTestCasesForApp(message.getAppId(), message.getAccountId(), message.getSuiteId());
	}
	
	private void generateTestCasesForApp(String appId, String accountId, String suiteId) {
		Collection<Activity> sessionStarts = this.graphOperations.getSessionStarts(appId);

		int minHitsToTraversePath = this.calculateMinHits(suiteId, sessionStarts);

		Paths<Activity> paths = new Paths<Activity>();

		for (Activity sessionStart : sessionStarts) {
			paths.add(generatePathsFromStart(sessionStart, minHitsToTraversePath));
		}

	    this.storageRecorder.saveFromGraphToStorage(appId, accountId, sessionStarts, suiteId);

	    MongoCollection<Document> suites = this.database.getCollection("suites");
	    suites.updateOne(eq("id", suiteId), set("lastUpdated", new java.util.Date()));
	}
	
	private int calculateMinHits(String suiteId, Collection<Activity> sessionStarts) {
		MongoCollection<Document> suites = this.database.getCollection("suites");
		Document suite = suites.find(eq("id", suiteId)).first();
		Integer percentage = suite.getInteger("usagePercentage");

		int sessions = 0;
		for (Activity activity : sessionStarts) {
			sessions += activity.getSessionStartHits();
		}

		return Math.round((percentage / 100F) * sessions); 
	}

	private Path<Activity> generatePathsFromStart(Activity sessionStart, int minHitsToTraversePath) {
		Path<Activity> startPath = new Path<Activity>(sessionStart);

		Iterable<Impact> startImpacts = this.graphOperations.getSessionStartImpacts(sessionStart, minHitsToTraversePath);

		for (Impact startImpact : startImpacts) {
			this.generatePathWithBranches(startPath.getLast(), startImpact, minHitsToTraversePath);
		}

		return startPath;
	}

	/*
	 * Generates tree of paths 
	 */
	private void generatePathWithBranches(PathNode<Activity> lastPathEnd, Impact startImpact, int minHitsToTraversePath) {
		Path<Activity> pathToEnd = new Path<Activity>();

		lastPathEnd.getChildren().add(pathToEnd);

		pathToEnd.setPathId(startImpact.getPathId());

		Iterable<Impact> pathImpacts = this.graphOperations.findPath(startImpact.getAppId(), startImpact.getPathId());

		Iterator<Impact> pathIterator = pathImpacts.iterator();
		while (pathIterator.hasNext()) {
			Impact impact = pathIterator.next();
			Activity currentActivity = impact.getEffect();
			PathNode<Activity> currentNode = pathToEnd.add(currentActivity);
		
			List<Impact> divergences = this.getDivergences(currentActivity, startImpact.getPathId(), impact.getCyclesNumber(), minHitsToTraversePath);

			for (Impact divergence : divergences) {
				this.generatePathWithBranches(currentNode, divergence, minHitsToTraversePath);
			}
		}

		
		this.graphRecorder.saveScenario(pathToEnd, startImpact);
	}

	private List<Impact> getDivergences(Activity node, String prevPathId, int prevCycleNumber, int minHits) {
		return node.getImpacts().stream().filter(
				x -> prevPathId.equals(x.getPreviousPathId()) &&
					 x.isPathStart() == true &&
					 x.getCyclesNumber() - prevCycleNumber >= 0 &&
					 x.getCyclesNumber() - prevCycleNumber <= 1 &&
					 x.getHits() >= minHits).collect(Collectors.toList());
	}

	private MongoDatabase database;
	private Session graphSession;
	private GraphOperations graphOperations;
	private ScenariosGraphRecorder graphRecorder;
	private ScenariosStorageRecorder storageRecorder;
}
