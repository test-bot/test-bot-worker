package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class ScenariosGraphRecorder {
	@Inject
	public ScenariosGraphRecorder(GraphSessionFactory sessionFactory) {
		this.graphSession = sessionFactory.getSession();
	}

	public void saveScenario(Path<Activity> path, Impact divergenceImpact) {
		Transaction t = this.graphSession.beginTransaction();

		try {
			String scenarioId;
			if(divergenceImpact.getGeneratedScenarioId() == null) {
				scenarioId = UUID.randomUUID().toString();
				divergenceImpact.setGeneratedScenarioId(scenarioId);
				this.graphSession.save(divergenceImpact);
			} else {
				scenarioId = divergenceImpact.getGeneratedScenarioId();
			}

			this.saveScenariosToImpacts(path.getLast(), scenarioId);
	
			t.commit();
		}
		catch (Exception ex) {
			t.rollback();
			throw new RuntimeException("Error when trying to save scenario in the graph.", ex);
		}

		t.close();
	}
	
	private void saveScenariosToImpacts(PathNode<Activity> node, String... scenarioIds) {
		PathNode<Activity> currentNode = node;
		PathNode<Activity> previousNode = node.getPrevious();
		
		while(previousNode != null) {
			Optional<Impact> impact = this.getImpactBetweenTwoNodes(previousNode, currentNode);

			if(impact.isPresent()) {
				List<String> ids = impact.get().getScenariosIds();
				if(ids == null) {
					ids = new ArrayList<>();
					impact.get().setScenariosIds(ids);
				}
				
				for (String id : scenarioIds) {
					if(!ids.contains(id)) {
						ids.add(id);
					}
				}
			}

			this.graphSession.save(impact.get());

			currentNode = previousNode;
			previousNode = currentNode.getPrevious();
		}
	}

	private Optional<Impact> getImpactBetweenTwoNodes(PathNode<Activity> previousNode, PathNode<Activity> currentNode) {
		return previousNode.getValue()
				.getImpacts()
				.stream()
				.filter(i -> i.getEffect().equals(currentNode.getValue()) &&
						i.getPathOrdinal() == currentNode.getOrdinal())
				.findFirst();
	}

	private Session graphSession;
}
