package com.usabilitics.testbot.jobs.scenariosGenerator;

public class PathNode<T> {
	private PathNode<T> previous;
	private PathNode<T> next;
	
	private T value;
	
	private String suiteId;

	private Paths<T> children;

	private int ordinal;

	public PathNode(T value) {
		this.children = new Paths<T>();
		this.children.setStart(this);
		this.setValue(value);
	}

	/**
	 * @return the previous
	 */
	public PathNode<T> getPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(PathNode<T> previous) {
		this.previous = previous;
	}

	/**
	 * @return the next
	 */
	public PathNode<T> getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(PathNode<T> next) {
		this.next = next;
	}

	/**
	 * @return the value
	 */
	public T getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}

	/**
	 * @return the suiteId
	 */
	public String getSuiteId() {
		return suiteId;
	}

	/**
	 * @param suiteId the suiteId to set
	 */
	public void setSuiteId(String suiteId) {
		this.suiteId = suiteId;
	}
	
	/**
	 * @return the children
	 */
	public Paths<T> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(Paths<T> children) {
		this.children = children;
	}

	/**
	 * @return the ordinal
	 */
	public int getOrdinal() {
		return ordinal;
	}

	/**
	 * @param ordinal the ordinal to set
	 */
	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
}
