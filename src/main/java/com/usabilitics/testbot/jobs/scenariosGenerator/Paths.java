package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.ArrayList;
import java.util.Iterator;

import com.usabilitics.testbot.model.graph.Activity;

/*
 * A collection of several Paths
 */
public class Paths<T> implements Iterable<Path<T>> {
	public void add(Path<T> path) {
		if(path.getFirst() != null) {
			path.getFirst().setPrevious(this.getStart());
		}
		path.setPrevious(this.getStart());
		this.paths.add(path);
	}
	
	public void add(Paths<T> paths) {
		for (Path<T> path : paths) {
			this.add(path);
		}
	}

	@Override
	public Iterator<Path<T>> iterator() {
		return this.paths.iterator();
	}

	/**
	 * @return the start
	 */
	public PathNode<T> getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(PathNode<T> start) {
		this.start = start;
	}

	private ArrayList<Path<T>> paths = new ArrayList<Path<T>>();
	private PathNode<T> start;
}
