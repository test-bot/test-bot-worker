package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.cypher.query.SortOrder;
import org.neo4j.ogm.cypher.query.SortOrder.Direction;
import org.neo4j.ogm.session.Session;

import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class GraphOperations {
	@Inject
	public GraphOperations(GraphSessionFactory factory) {
		this.graphSession = factory.getSession();
	}

	public Collection<Activity> getSessionStarts(String appId) {
		Filter startFilter = new Filter("sessionStartHits", 0);
		startFilter.setComparisonOperator(ComparisonOperator.GREATER_THAN);
		startFilter.setBooleanOperator(BooleanOperator.AND);
		
		Filter appFilter = new Filter("appId", appId);
		appFilter.setBooleanOperator(BooleanOperator.AND);
		
		Filters filters = new Filters();
		filters.add(startFilter, appFilter);

		SortOrder sort = new SortOrder();
		sort.add(Direction.DESC, "sessionStartHits");

		Collection<Activity> sessionStarts = this.graphSession.loadAll(Activity.class, filters, sort);
		return sessionStarts;
	}

	public Iterable<Impact> getSessionStartImpacts(Activity sessionStart, int minHits) {
		String cypherQuery = 
				"MATCH (ss{domainId: {nodeId}})-[r:IMPACTS{isAfterSessionStart:true}]->() " +
				"WHERE r.hits >= {minHits} " + 
				"RETURN r " +
				"ORDER BY r.hits DESC";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("nodeId", sessionStart.getDomainId());
		params.put("minHits", minHits);

		return this.graphSession.query(Impact.class, cypherQuery, params);
	}
	
	private Session graphSession;

	public Iterable<Impact> findPath(String appId, String pathId) {
		Filters filters = new Filters();
		Filter appFilter = new Filter("appId", appId);
		appFilter.setBooleanOperator(BooleanOperator.AND);
		filters.add(
				new Filter("pathId", pathId),
				appFilter);

		Collection<Impact> result = this.graphSession.loadAll(Impact.class, filters, new SortOrder().add("pathOrdinal"));

		ArrayList<Impact> sorted = new ArrayList<Impact>(result);
		Collections.sort(sorted, this.comparator);

		return sorted;
	}

	public Iterable<Impact> getScenario(String scenarioId, String appId) {
		String cypherQuery = 
				"MATCH ({appId: {appId}})-[r:IMPACTS{appId: {appId}}]->({appId: {appId}}) " +
				"WHERE {scenarioId} IN r.scenariosIds " + 
				"RETURN r " +
				"ORDER BY r.pathOrdinal";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("appId", appId);
		params.put("scenarioId", scenarioId);
		
		return this.graphSession.query(Impact.class, cypherQuery, params);
	}

	private class ImpactsComparator implements Comparator<Impact> {
		@Override
		public int compare(Impact o1, Impact o2) {
			return Integer.compare(o1.getPathOrdinal(), o2.getPathOrdinal());
		}
	}

	private final ImpactsComparator comparator = new ImpactsComparator();
}
