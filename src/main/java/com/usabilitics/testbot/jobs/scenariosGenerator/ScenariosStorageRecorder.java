package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.*;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.model.graph.Activity;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class ScenariosStorageRecorder {
	@Inject
	public ScenariosStorageRecorder(DatabaseClient database, GraphOperations graphOperations) {
		MongoDatabase db = database.getClient().getDatabase("test-bot");
		this.scenariosCollection = db.getCollection("scenarios");
		this.graphOperations = graphOperations;
	}

	public void saveFromGraphToStorage(String appId, String accountId, Collection<Activity> starts, String suiteId) {
		List<String> scenariosIds = new ArrayList<String>();

		StreamSupport.stream(starts.spliterator(), false)
		.flatMap(s -> StreamSupport.stream(s.getImpacts().spliterator(), false))
		.filter(i -> i.isAfterSessionStart())
		.collect(
				Collectors.reducing(scenariosIds, i -> i.getScenariosIds(), (all, ids)-> {
					all.addAll(ids);
					return all;
				}));
		
		this.saveFromGraphToStorage(appId, accountId, scenariosIds, suiteId);
	}

	public void saveFromGraphToStorage(String appId, String accountId, List<String> scenariosIds, String suiteId) {
		for (String scenarioId : scenariosIds) {
			Iterable<Impact> scenarioImpacts = this.graphOperations.getScenario(scenarioId, appId);

			Document scenario = new Document();
			scenario.append("scenarioId",scenarioId);
			scenario.append("createdOn", new Date());
			scenario.append("appId", appId);
			scenario.append("suiteId", suiteId);

			Document acl = new Document();
			ArrayList<String> list = new ArrayList<String>();
			list.add(accountId);
			acl.append("canRead", list);
			acl.append("canUpdate", list);

			scenario.append("acl", acl);

			ArrayList<Document> steps = new ArrayList<Document>();
			for (Impact impact : scenarioImpacts) {
				if (impact.isAfterSessionStart()) {
					Activity node = impact.getCause();
					this.addNodeToSteps(node, steps);
				}

				Activity node = impact.getEffect();
				this.addNodeToSteps(node, steps);
			}

			scenario.append("steps", steps);

			this.scenariosCollection.deleteOne(eq("scenarioId", scenarioId));
			this.scenariosCollection.insertOne(scenario);
		}
	}

	private void addNodeToSteps(Activity node, ArrayList<Document> steps) {
		Document doc = HelperMethods.createDocumentFromActivity(node);
		doc.append("stepId", UUID.randomUUID().toString());
		steps.add(doc);
	}

	private MongoCollection<Document> scenariosCollection;
	private GraphOperations graphOperations;
}
