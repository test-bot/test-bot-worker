package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.util.Iterator;

public class PathIterator<T extends PathNode<?>> implements Iterator<T> {
	public PathIterator(T node) {
		this.current = node;
	}

	@Override
	public boolean hasNext() {
		return this.current != null;
	}

	@Override
	public T next() {
		T next = this.current;
		this.current = (T) next.getNext();
		return next;
	}

	private T current;

}
