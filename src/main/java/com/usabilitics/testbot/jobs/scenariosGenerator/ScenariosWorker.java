package com.usabilitics.testbot.jobs.scenariosGenerator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.usabilitics.testbot.connectivity.database.DatabaseClient;
import com.usabilitics.testbot.connectivity.graph.GraphSessionFactory;
import com.usabilitics.testbot.messages.DeleteScenarioMessage;
import com.usabilitics.testbot.model.graph.Impact;

@Named
public class ScenariosWorker {

	@Inject
	public ScenariosWorker(GraphSessionFactory graphSessionFactory, DatabaseClient db, GraphOperations graphOperations) {
		this.database = db.getClient().getDatabase("test-bot");
		this.graphSession = graphSessionFactory.getSession();
		this.graphOperations = graphOperations;
		this.scenariosCollection = this.database.getCollection("scenarios");
	}
	
	public void  deleteScenario(DeleteScenarioMessage message) {
		Logger logger = Logger.getLogger("jobs");
		logger.log(Level.FINER, "DeleteScenario task started:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));

		Transaction t = this.graphSession.beginTransaction();

		try {
			Iterable<Impact> scenarioImpacts = this.graphOperations.getScenario(message.getScenarioId(), message.getAppId());
			for (Impact impact : scenarioImpacts) {
				impact.getScenariosIds().remove(message.getScenarioId());
				if(message.getScenarioId().equals(impact.getGeneratedScenarioId())) {
					impact.setGeneratedScenarioId(null);
				}
			}

			this.scenariosCollection.deleteOne(new Document("scenarioId", message.getScenarioId()));

			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new RuntimeException("Error when deleting scenarios from the graph.", e);
		} finally {
			logger.log(Level.FINER, "DeleteScenario task ended:" + new SimpleDateFormat("HH:mm:ss.SSSS dd.MM.yyyy").format(Calendar.getInstance().getTime()));
		}
		
		t.close();
	}

	
	private MongoDatabase database;
	private Session graphSession;
	private GraphOperations graphOperations;
	private MongoCollection<Document> scenariosCollection;
}
