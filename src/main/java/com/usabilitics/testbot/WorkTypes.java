package com.usabilitics.testbot;

public enum WorkTypes {
	PromoteForSessionCalculation,
	CalculateSessions,
	NewSession,
	ApplySettingsChanges,
	CreateApplication,
	GenerateScenarios,
	RecognizeInputDataRequirements,
	DeleteSuite,
	DeleteScenario,
	DeleteApplication
}
